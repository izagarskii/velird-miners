﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

    public GameObject abilitiesHolderParent;
    private GameObject abilitiesHolder;
    public GameObject abilitiesHolderPrefab;
    public GameObject abilitiesButtonPrefab;

    public GameObject hardnessUIPrefab;
    public GameObject selectionUIPrefab;

    public GameObject moveTargetUIPrefab;
    public GameObject digTargetUIPrefab;
    public GameObject guardTargetUIPrefab;

    private GameObject currentSelection;

	public ActorStatsWindow actorStatsWindow;

    public static UIManager instance;

    void Start() {
        if (instance == null) {
            instance = this;
        }
    }

	public void SelectActor() {
		Actor subject = ActorManager.instance.GetSubject();
		ShowActorSelection (subject);
		actorStatsWindow.gameObject.SetActive (true);
		subject.ShowStats (actorStatsWindow);
        ShowAbilities(subject);
	}

	private void ShowActorSelection(Actor subject) {
        if (subject) {
            if (currentSelection) {
                Destroy(currentSelection);
            }
            GameObject selection = Instantiate(selectionUIPrefab, subject.transform);
            currentSelection = selection;
        }
    }

    public void ShowTarget(Vector3 position) {
        //Instantiate(ActorManager.instance.GetTaskFactory().GetTargetUIPrefab(), position, Quaternion.Euler(90, 0, 0));
        Instantiate(moveTargetUIPrefab, position, Quaternion.Euler(90, 0, 0));
    }

    private void ShowAbilities(Actor subject) {
        ClearAbilities();
        foreach (Ability a in subject.GetAbilitities()) {
            CreateAbilityButton(a);
        }
    }

    public void CreateAbilityButton(Ability ability) {
        if (abilitiesHolder) {
            Vector3 position = new Vector3(abilitiesHolder.transform.position.x + 80 * abilitiesHolder.transform.childCount, 5, 0);
            AbilityButton ab = Instantiate(abilitiesButtonPrefab, position,
                                            Quaternion.identity, abilitiesHolder.transform).GetComponent<AbilityButton>();
            ab.SetAbility(ability);
        }
    }

    private void ClearAbilities() {
        if (abilitiesHolder) {
            Destroy(abilitiesHolder);
        }
        abilitiesHolder = Instantiate(abilitiesHolderPrefab, abilitiesHolderParent.transform);
    }
}
