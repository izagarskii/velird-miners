﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeamManager : MonoBehaviour
{
    public static TeamManager instance;
    public Text goldText;

    [SerializeField] private int gold;

    private int glory;
    private int gloryLevel = 1;
    public Text gloryLevelText;
    public Text gloryText;
    public Text donationText;

    public float gloryTickRate;
    public float donationTickRate;
    public int donationUnit = 150;

    private float nextGloryTick = 0.0f;
    private float nextDonationTick = 0.0f;

    public void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        nextDonationTick = GetNextDonationTick();
        RefreshGoldUI();
    }

    public void Update()
    {
        if (Time.time > nextGloryTick)
        {
            nextGloryTick = GetNextGloryTick();
            ChangeGlory(-1);
        }
        if (Time.time > nextDonationTick)
        {
            nextDonationTick = GetNextDonationTick();
            MakeDonation();
        }
    }

    public int GetGold() {
        return gold;
    }

    public bool ChangeGold(int delta) {
        if (gold + delta < 0) {
            return false;
        }
        gold += delta;
        RefreshGoldUI();
        return true;
    }

    public void ChangeGlory(int delta)
    {
        glory += delta;
        if (glory < 0)
        {
            glory = 0;
        }
        else if (glory > GetGloryNeeded()) {
            GloryLevelUp();
        }
        RefreshGloryUI();
    }

    public int GetGlory() {
        return glory;
    }

    public int GetGloryNeeded() {
        return gloryLevel * 100;
    }

    private void GloryLevelUp() {
        ChangeGlory(-GetGloryNeeded());
        gloryLevel++;
    }

    private int GetDonation() {
        return donationUnit * gloryLevel;
    }
    private int GetDonationTime()
    {
        return (int)(nextDonationTick - Time.time);
    }

    private void MakeDonation() {
        ChangeGold(GetDonation());
    }

    public void RefreshGoldUI() {
        goldText.text = "" + gold;
    }

    public void RefreshGloryUI()
    {
        gloryLevelText.text = "Glory level: " + gloryLevel;
        gloryText.text = "Glory: " + glory + " of " + GetGloryNeeded();
        donationText.text = "Next Donation (" + GetDonation() + ") in " + GetDonationTime() + " seconds!";
    }

    private float GetNextGloryTick()
    {
        return Time.time + gloryTickRate;
    }

    private float GetNextDonationTick()
    {
        return Time.time + donationTickRate;
    }
}
