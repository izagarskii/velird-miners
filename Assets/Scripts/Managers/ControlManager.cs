﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlManager : MonoBehaviour {

    public float cameraSpeed;
    public GameObject parallaxBackground;
    public float parallaxSpeed;
    public GameObject pauseMenu;

    private ConstructionManager constructionManager;

    private bool rotateFired;

    void Start() {
        constructionManager = GetComponent<ConstructionManager>();
    }

    void Update()
    {
        float cameraResultSpeed = cameraSpeed * Time.deltaTime;

        if (Input.GetKey("d"))
        {
            transform.Translate(cameraResultSpeed, 0, 0);
            parallaxBackground.transform.Translate(cameraResultSpeed * parallaxSpeed, 0, 0);
        }

        if (Input.GetKey("a"))
        {
            transform.Translate(-cameraResultSpeed, 0, 0);
            parallaxBackground.transform.Translate(-cameraResultSpeed * parallaxSpeed, 0, 0);
        }

        if (Input.GetMouseButtonDown(1) && !pauseMenu.gameObject.activeInHierarchy)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            LayerMask mask = 1 << 9;

            if (Physics.Raycast(ray, out hit, 200, mask))
            {
                ActorManager.instance.UpdateSubjectTask(hit.point);
            }
        }

        if (Input.GetKey("r"))
        {
            rotateFired = true;
        }
        else if (rotateFired) {
            rotateFired = false;
            ConstructionManager.instance.RotateConstruction();
        }

        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (!pauseMenu.activeInHierarchy)
            {
                PauseGame();
            } else if (pauseMenu.activeInHierarchy)
            {
                ContinueGame();
            }
        }
    }

    public void Construct(GameObject template)
    {
        constructionManager.CancelConstruction();
        constructionManager.SelectConstructPlace(template.GetComponent<ConstructionTemplate>());
    }

    private void PauseGame()
    {
        Time.timeScale = 0;
        pauseMenu.SetActive(true);
        constructionManager.CancelConstruction();
        //Disable scripts that still work while timescale is set to 0
    }
    private void ContinueGame()
    {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
        //enable the scripts again
    }
}
