﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstructionManager : MonoBehaviour {

    public static ConstructionManager instance;
    //public ConstructionTemplate axeTrapTemplate;
    public GameObject constructButton;
    private Hero constructor;
    private GameObject currentConstruction;

    public void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void SelectConstructPlace(ConstructionTemplate template) {
        if (constructor && TeamManager.instance.GetGold() >= template.cost)
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            currentConstruction = Instantiate(template.gameObject, pos, Quaternion.Euler(90, 0, 0));
            currentConstruction.GetComponent<ConstructionTemplate>().SetBuilder(constructor);
        }
    }

    public void CancelConstruction() {
        if (currentConstruction) {
            Destroy(currentConstruction);
        }
    }

    public void SetUpConstructor(Actor actor) {
        if (actor is Hero)
        {
            constructor = actor as Hero;
            constructButton.SetActive(true);
        }
        else {
            CancelConstruction();
        }
        if (ActorManager.instance.GetSubject() && ActorManager.instance.GetSubject().isPassive()) {
            constructButton.SetActive(false);
        }
    }

    public void RotateConstruction() {
        if (currentConstruction && currentConstruction.GetComponent<ConstructionTemplate>().rotatable)
        {
            currentConstruction.transform.Rotate(new Vector3(0, 0, -90));
        }
    }
}