﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChosenButton : MonoBehaviour
{
    public GameObject heroObject;
    public Image buttonImage;
    public ActorStatsWindow statsWindow;

    public Hero hero;

    private void Start()
    {
        hero = heroObject.GetComponent<Hero>();
        hero.ShowStats(statsWindow);
    }
}
