﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMissionButton : MonoBehaviour
{
    public List<Vector3> startPositions = new List<Vector3>();
    private List<GameObject> teamHeroesList = new List<GameObject>();
    private int sceneIndex;
    private AsyncOperation sceneAsync;

    public void SaveTeam() {
        foreach (Hero h in CityManager.instance.GetTeam()) {
            teamHeroesList.Add(h.gameObject);
        }
    }

    public void StartMission()
    {
        SaveTeam();
        sceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
        StartCoroutine(loadScene(sceneIndex));
    }

    IEnumerator loadScene(int index)
    {
        AsyncOperation scene = SceneManager.LoadSceneAsync(index, LoadSceneMode.Additive);
        scene.allowSceneActivation = false;
        sceneAsync = scene;

        //Wait until we are done loading the scene
        while (scene.progress < 0.9f)
        {
            Debug.Log("Loading scene " + " [][] Progress: " + scene.progress);
            yield return null;
        }
        OnFinishedLoadingAllScene();
    }

    IEnumerator enableScene(int index)
    {
        //Activate the Scene
        sceneAsync.allowSceneActivation = true;

        while (!sceneAsync.isDone)
        {
            // wait until it is really finished
            yield return null;
        }

        Scene sceneToLoad = SceneManager.GetSceneByBuildIndex(index);
        if (sceneToLoad.IsValid())
        {
            Debug.Log("Scene is Valid");
            foreach (GameObject h in teamHeroesList) {
                SceneManager.MoveGameObjectToScene(h, sceneToLoad);
            }
            Scene prevScene = SceneManager.GetActiveScene();
            SceneManager.SetActiveScene(sceneToLoad);
            Debug.Log("Scene Activated!");
            foreach (GameObject r in sceneToLoad.GetRootGameObjects()) {
                Debug.Log(r.name);
                if (teamHeroesList.Contains(r)) {
                    Instantiate(r, startPositions[teamHeroesList.IndexOf(r)], r.transform.rotation);
                }
            }
            SceneManager.UnloadSceneAsync(prevScene);
            GameManager.instance.StartGame();
        }
    }

    void OnFinishedLoadingAllScene()
    {
        Debug.Log("Done Loading Scene");
        StartCoroutine(enableScene(sceneIndex));
    }
}
