﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CityManager : MonoBehaviour
{
    public static CityManager instance;

    public int maxHeroCount;
    public Color defaultChosenButtonColor;
    public Button startButton;

    private List<Hero> chosen = new List<Hero>();

    public void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        UpdateStartButton();
    }

    public void ChooseHero(ChosenButton button) {
        if (chosen.Contains(button.hero))
        {
            chosen.Remove(button.hero);
        }
        else if (chosen.Count < maxHeroCount) {
            chosen.Add(button.hero);
        }

        if (chosen.Contains(button.hero))
        {
            button.buttonImage.color = Color.white;
        }
        else
        {
            button.buttonImage.color = defaultChosenButtonColor;
        }
        UpdateStartButton();
    }

    public List<Hero> GetTeam() {
        return chosen;
    }

    private void UpdateStartButton() {
        startButton.gameObject.SetActive(chosen.Count == maxHeroCount);
    }
}
