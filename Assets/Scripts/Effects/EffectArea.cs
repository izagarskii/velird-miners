﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectArea : MonoBehaviour
{
    public Effect effectToCall;
    private EffectArea parentArea;
    private List<Actor> affected = new List<Actor>();

    // Start is called before the first frame update
    void Start()
    {
        if (effectToCall != null) {
            foreach (EffectArea a in transform.GetComponentsInChildren<EffectArea>())
            {
                SetParentArea(this);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Actor actor = other.GetComponent<Actor>();
        if (parentArea && actor != null && !parentArea.affected.Contains(actor))
        {
            parentArea.effectToCall.Affect(actor);
            parentArea.affected.Add(actor);
        }
    }

    protected void SetParentArea(EffectArea parent) {
        this.parentArea = parent;
    }
}
