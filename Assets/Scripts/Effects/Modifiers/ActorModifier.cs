﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorModifier : MonoBehaviour
{
    public float lifetime;
    public float frequency;
    public Sprite sprite;
    protected Actor subject;

    private float nextTick;
    private float endLife;

    public void SetSubject(Actor subject) {
        this.subject = subject;
    }

    protected virtual void Tick() { 
    
    }

    void Start()
    {
        endLife = Time.time + lifetime;
        Begin();
    }

    // Update is called once per frame
    public void Update()
    {
        if (frequency != 0) {
            if (Time.time >= nextTick) {
                UpdateNextTick();
                Tick();
            }
        }
        if (Time.time >= endLife) {
            End();
        }
    }

    protected virtual void Begin()
    {
        
    }

    protected virtual void End() {
        Destroy(gameObject);
    }
    private void UpdateNextTick()
    {
        nextTick = Time.time + frequency;
    }
}
