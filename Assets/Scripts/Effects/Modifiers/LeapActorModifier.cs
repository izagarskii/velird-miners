﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LeapActorModifier : TargetActorModifier
{
    private NavMeshAgent agent;
    private SpriteRenderer renderer;

    protected override void Begin()
    {
        base.Begin();
        Debug.Log(name + " begins");
        agent = subject.GetComponent<NavMeshAgent>();
        renderer = subject.GetComponentInChildren<SpriteRenderer>();

        agent.enabled = false;
        subject.transform.localScale = new Vector3(1.5f, 0.1f, 1.5f);
        renderer.sortingLayerName = "UI";
        renderer.sortingOrder = 1;
    }

    protected override void End() {
        Debug.Log(name + " ends");
        agent.enabled = true;
        subject.transform.localScale = new Vector3(1.0f, 0.1f, 1.0f);
        renderer.sortingLayerName = "people";
        renderer.sortingOrder = 0;
        base.End();
    }

    new public void Update()
    {
        base.Update();
        subject.transform.position = Vector3.MoveTowards(subject.transform.position, targetPosition, 5 * Time.deltaTime);
    }
}
