﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetActorModifier : ActorModifier
{
    protected Vector3 targetPosition;

    public void SetTargetPosition(Vector3 pos) {
        targetPosition = pos;
    }


}
