﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public int radius;
    public int damage;
    public int rockCrushRate;
    public int collapseRate;
    public GameObject collapse;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Explosion!");
        transform.localScale = new Vector3(radius * 7, radius * 7, 1);
        Vector3[] directions = { Vector3.right, Vector3.left, Vector3.forward, Vector3.back,
                                 Vector3.right, Vector3.left, Vector3.forward, Vector3.back,
                                 new Vector3(1,0,1), new Vector3(1,0,-1), new Vector3(-1,0,1), new Vector3(-1,0,-1) ,
                                 new Vector3(2,0,1), new Vector3(2,0,-1), new Vector3(1,0,-2), new Vector3(-1,0,-2) ,
                                 new Vector3(1,0,2), new Vector3(-1,0,2), new Vector3(-2,0,1), new Vector3(-2,0,-1) };
        RaycastHit hit;
        int wallLayerMask = 1 << 14;
        int unitsLayerMask = 1 << 15;

        foreach (Vector3 dir in directions)
        {
            for (int i = 0; i < 2; i++) {
                if (Physics.Raycast(transform.position, dir, out hit, radius, wallLayerMask))
                {
                    Digable toCrush = hit.collider.gameObject.GetComponent<Digable>();
                    if (toCrush && Random.Range(0,100) < rockCrushRate)
                    {
                        toCrush.Die();
                    }
                }
            }
            //Debug.DrawRay(transform.position, dir, Color.red, 5.0f);
            if (Physics.Raycast(transform.position, dir, out hit, radius, unitsLayerMask))
            {
                Suffer(hit);
            }
        }

        for (int i = 0; i < 3; i++) {
            if (Physics.Raycast(transform.position - Vector3.up, Vector3.up, out hit, radius, unitsLayerMask))
            {
                Suffer(hit);
            }
        }

        int collapseRadius = radius - 1;
        List<Vector3> poses = new List<Vector3>();
        for (int i = 0; i < radius * 2; i++)
        {
            if (Random.Range(0, 100) < collapseRate)
            {
                int x = (int) (transform.position.x + Random.Range(-collapseRadius, collapseRadius));
                int z = (int) (transform.position.z + Random.Range(-collapseRadius, collapseRadius));
                Vector3 pos = new Vector3(x, 0f, z);
                if (!poses.Contains(pos)) {
                    poses.Add(pos);
                    Instantiate(collapse, pos, collapse.transform.rotation);
                }
            }
        }
    }

    private void Suffer(RaycastHit hit) {
        hit.collider.gameObject.GetComponent<Actor>().Suffer(damage);
    } 
}
