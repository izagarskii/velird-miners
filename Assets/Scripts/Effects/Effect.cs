﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour
{
    protected Vector3 direction = Vector3.left;

    public void SetDirection(Vector3 dir)
    {
        direction = dir;
    }

    public void SetDirection(float angle)
    {
        direction = new Vector3 (-Mathf.Cos(angle), 0, Mathf.Sin(angle));
    }

    public virtual void Affect(Actor affected) {
        Debug.Log(affected.name + " is affected by " + name);
    }
}
