﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollapseEffect : TemporalEffect
{
    public GameObject rockMass;

    public override void OnEffectEnd()
    {
        Instantiate(rockMass, transform.position, rockMass.transform.rotation);
        base.OnEffectEnd();
    }

    public override void Affect(Actor affected)
    {
        base.Affect(affected);
        affected.Die();
    }
}
