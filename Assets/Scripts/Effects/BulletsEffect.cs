﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletsEffect : TemporalEffect
{
	public int damage;
	public int range;

	public new void Start()
	{
		base.Start();
		int unitsLayerMask = 1 << 15;
		RaycastHit hit;
		//Debug.Log(transform.position + ", " + (transform.position + direction));
		//Debug.DrawLine(transform.position, transform.position + direction * range, Color.red,1000f);
		if (Physics.Raycast(transform.position, direction, out hit, range, unitsLayerMask))
		{
			Actor actor = hit.collider.gameObject.GetComponent<Actor>();
			Affect(actor);
		}
	}

	public override void Affect(Actor affected)
	{
		base.Affect(affected);
		affected.Suffer(damage);
	}
}
