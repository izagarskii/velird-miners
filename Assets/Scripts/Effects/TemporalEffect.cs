﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemporalEffect : Effect {

    public float lifetime;

    private float deathTimeStamp;

    public void Start()
    {
        deathTimeStamp = Time.time + lifetime;
    }

    void Update () {
        if (Time.time > deathTimeStamp) {
            OnEffectEnd();
        }
	}

    public virtual void OnEffectEnd() {
        Destroy(gameObject);
    }
}
