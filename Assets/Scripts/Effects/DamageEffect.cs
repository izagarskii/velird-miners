﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageEffect : TemporalEffect {

	public int damage;

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Enemy")
		{
			Affect(other.GetComponent<Actor>());
		}
	}

	public override void Affect(Actor affected)
	{
		base.Affect(affected);
		affected.Suffer(damage);
	}
}
