﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : Movable {

    public string heroName;
    public int pick;
    public int build;
    public int charisma;
    public GameObject attackEffect;

    public Weapon weapon;
    public Weapon fists;

    private GuardTask guardTask;

    protected new void Start()
    {
        base.Start();
        guardTask = new GuardTaskFactory().CreateTask(transform.position, this) as GuardTask;
    }

    public override void Attack(Actor enemy)
    {
        if (weapon)
        {
            Instantiate(weapon.vfx, attackPivot);
        }
        else {
            Instantiate(fists.vfx, attackPivot);
        }

        Instantiate(attackEffect, enemy.transform.position, Quaternion.Euler(90, 0, 0));
        if (enemy.Suffer(GetDamage())) {
            TeamManager.instance.ChangeGlory(enemy.YieldGlory(charisma));
        }
    }

    public override bool isPassive()
    {
        return false;
    }

    public override void Die() {
        GameManager.instance.heroes.Remove(this);
        Destroy(gameObject);
    }

    public override void Perform() {
        base.Perform();
        if (!IsBusy()) {
            guardTask.Act();
        }
    }

	public override void ShowStats(ActorStatsWindow statsWindow) {
		statsWindow.actorName.text = heroName;
		statsWindow.image.sprite = GetComponentInChildren<SpriteRenderer> ().sprite;
		statsWindow.stats.text =
            "Weapon : " + GetWeaponName() + "\n" +
            "Damage : " + GetDamage() + "\n" +
            "Fire rate : " + GetFireRateDesc() + "\n" +
            "Attack range : " + GetAttackRange() + "m\n" +
            "\n" +
			"Dig : " + pick + "\n" +
			"Construct : " + build + "\n" +
            "Charisma : " + charisma + "\n" +
            "Health : " + GetMaxHP() + "\n";
    }

    public float GetFireRate() {
        if (weapon)
        {
            return weapon.fireRate;
        }
        return fists.fireRate;
    }

    public override float GetAttackRange()
    {
        if (weapon)
        {
            return weapon.attackRange;
        }
        return fists.attackRange;
    }

    private int GetDamage()
    {
        if (weapon)
        {
            return weapon.damage;
        }
        return fists.damage;
    }

    private string GetFireRateDesc()
    {
        if (weapon)
        {
            return weapon.fireRateDesc;
        }
        return fists.fireRateDesc;
    }

    private string GetWeaponName()
    {
        if (weapon)
        {
            return weapon.weaponName;
        }
        return fists.weaponName;
    }
}
