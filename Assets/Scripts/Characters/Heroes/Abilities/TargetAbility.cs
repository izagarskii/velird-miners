﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetAbility : Ability
{
    public TargetAbilityTemplate templatePrefab;
    public TargetActorModifier modifier;
    public float range;

    public void TargetEffect(Vector3 pos) {
        TargetActorModifier m = Instantiate(modifier, actor.transform);
        m.SetSubject(actor);
        m.SetTargetPosition(pos);
    }

    public override void MakeEffect() {
        base.MakeEffect();
        TargetAbilityTemplate t = Instantiate(templatePrefab, transform.position, templatePrefab.transform.rotation)
            .GetComponent<TargetAbilityTemplate>();
        t.SetAbility(this);
    }

    public override void Pay()
    {
        
    }

    public void PayAfterPlace() {
        base.Pay();
    }
}
