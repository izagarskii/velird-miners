﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability : MonoBehaviour
{
    public Sprite sprite;
    public string abilityName;
    public float cooldown;
    public Ability attachedAbility;
    public GameObject effect;

    protected Actor actor;

    private float cooldownTimestamp;

    public void Activate() {
        if (!isOnCooldown() && ifCanUse()) {
            Pay();
            if (attachedAbility)
            {
                attachedAbility.MakeEffect();
            }
            MakeEffect();
        }
    }

    public virtual void Pay() {
        cooldownTimestamp = Time.time + cooldown;
    }

    public virtual bool ifCanUse() {
        return true;
    }

    public virtual void MakeEffect() {
        Debug.Log(abilityName + " is activated! " + Time.time);
        if (effect) {
            Instantiate(effect, actor.transform.position, effect.transform.rotation);
        }
    }

    public void SetActor(Actor actor) {
        this.actor = actor;
    }

    public int Tick()
    {
        int delta = (int)(cooldownTimestamp - Time.time);
        if (delta <= 0) {
            return 0;
        }
        return delta;
    }

    public bool isOnCooldown() {
        return cooldownTimestamp > Time.time;
    }

    public Vector3 GetCastPosition() {
        return actor.transform.position;
    }
}
