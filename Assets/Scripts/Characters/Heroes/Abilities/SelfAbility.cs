﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfAbility : Ability
{
    public ActorModifier modifier;

    public override void MakeEffect()
    {
        base.MakeEffect();
        ActorModifier m = Instantiate(modifier, actor.transform);
        m.SetSubject(actor);
    }
}
