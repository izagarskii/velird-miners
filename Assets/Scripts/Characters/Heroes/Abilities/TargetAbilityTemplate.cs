﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TargetAbilityTemplate : MonoBehaviour, IPointerClickHandler
{
    private List<Collider> touchers = new List<Collider>();
    private bool positionIsOk = false;
    private bool rangeIsOk = false;
    private TargetAbility ability;
    private SpriteRenderer sRenderer;

    void Start()
    {
        sRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Wall")
        {
            touchers.Add(other);
            positionIsOk = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        touchers.Remove(other);
        if (touchers.Count <= 1)
        {
            positionIsOk = true;
        }
    }

    public void OnPointerClick(PointerEventData pointer)
    {
        if (pointer.button == PointerEventData.InputButton.Left && positionIsOk && rangeIsOk)
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            ability.TargetEffect(pos);
            ability.PayAfterPlace(); 
        }
        Destroy(gameObject);
    }
    void Update()
    {
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(pos.x, 0, pos.z);
        CheckRange(pos);
        sRenderer.color = positionIsOk && rangeIsOk ? Color.white : Color.red;
    }

    public void SetAbility(TargetAbility ability) {
        this.ability = ability;
    }

    private void CheckRange(Vector3 pos) {
        int wallsLayerMask = 1 << 14;
        RaycastHit hit;
        Vector3 castPos = new Vector3(ability.GetCastPosition().x, 0, ability.GetCastPosition().z);
        Vector3 targetPos = new Vector3(pos.x, 0, pos.z);
        float distance = Vector3.Distance(castPos, targetPos);
        Vector3 direction = (targetPos - castPos) / distance;

        if (Physics.Raycast(castPos, direction, out hit, distance, wallsLayerMask))
        {
            rangeIsOk = false;
        }
        else {
            rangeIsOk = distance < ability.range;
        }
    }
}
