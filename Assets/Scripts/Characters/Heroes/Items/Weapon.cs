﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Item
{
    public string weaponName;
    public float attackRange;
    public float fireRate;
    public int damage;
    public string fireRateDesc;
    public GameObject vfx;
}
