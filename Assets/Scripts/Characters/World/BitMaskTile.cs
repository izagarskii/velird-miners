﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BitMaskTile : Tile
{
    public List<Sprite> O_sprites;
    public List<Sprite> N_sprites;
    public List<Sprite> NW_sprites;
    public List<Sprite> W_sprites;
    public List<Sprite> SW_sprites;
    public List<Sprite> S_sprites;
    public List<Sprite> SE_sprites;
    public List<Sprite> E_sprites;
    public List<Sprite> NE_sprites;
    public List<Sprite> NS_sprites;
    public List<Sprite> EW_sprites;
    public List<Sprite> NEW_sprites;
    public List<Sprite> SEW_sprites;
    public List<Sprite> NSE_sprites;
    public List<Sprite> NSW_sprites;

    void Start() {
        UpdateSprite();
        UpdateNeighbours();
    }

    protected override Sprite GetSprite()
    {
        byte bitMask = 0;
        if (CheckDirection(Vector3.forward)) {
            bitMask += 1;
        }
        if (CheckDirection(Vector3.right))
        {
            bitMask += 2;
        }
        if (CheckDirection(Vector3.back))
        {
            bitMask += 4;
        }
        if (CheckDirection(Vector3.left))
        {
            bitMask += 8;
        }

        switch (bitMask) {
            case 0:
                return Utils.GetRandom(O_sprites);
            case 1:
                return Utils.GetRandom(N_sprites);
            case 2:
                return Utils.GetRandom(W_sprites);
            case 3:
                return Utils.GetRandom(NW_sprites);
            case 4:
                return Utils.GetRandom(S_sprites);
            case 5:
                return Utils.GetRandom(NS_sprites);
            case 6:
                return Utils.GetRandom(SW_sprites);
            case 7:
                return Utils.GetRandom(NSW_sprites);
            case 8:
                return Utils.GetRandom(E_sprites);
            case 9:
                return Utils.GetRandom(NE_sprites);
            case 10:
                return Utils.GetRandom(EW_sprites);
            case 11:
                return Utils.GetRandom(NEW_sprites);
            case 12:
                return Utils.GetRandom(SE_sprites);
            case 13:
                return Utils.GetRandom(NSE_sprites);
            case 14:
                return Utils.GetRandom(SEW_sprites);
            default:
                return Utils.GetRandom(sprites);
        }
    }

    public void UpdateNeighbours() {
        UpdateNeighbour(Vector3.forward);
        UpdateNeighbour(Vector3.right);
        UpdateNeighbour(Vector3.back);
        UpdateNeighbour(Vector3.left);
    }

    private bool CheckDirection(Vector3 direction) {
		int wallsLayerMask = 1 << 14;
		RaycastHit hit;
		return Physics.Raycast(transform.position, direction, out hit, 1, wallsLayerMask);
	}

    private void UpdateNeighbour(Vector3 direction)
    {
        int wallsLayerMask = 1 << 14;
        RaycastHit hit;
        if (Physics.Raycast(transform.position, direction, out hit, 1, wallsLayerMask)) {
            BitMaskTile mask = hit.collider.GetComponent<BitMaskTile>();
            if (mask)
            {
                mask.UpdateSprite();
            }
        }
    }
}
