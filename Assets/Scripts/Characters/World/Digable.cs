﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Digable : Actor
{
    public int hardness;
    private int hardnessSelf;
    private GameObject hardnessUI;
    private Transform hardnessIndicator;

    public int softness;
    public GameObject collapse;

    new void Start() {
        actorManager = FindObjectOfType<ActorManager>();
        hardnessSelf = hardness;
    }

    override public bool isPassive()
    {
        return true;
    }

    override protected void ActOn()
    {
        Task task = new DigTaskFactory().CreateTask(this, actorManager.GetSubject());
        actorManager.UpdateSubjectTask(task);
    }

    public bool Dig(int damage) {
        hardnessSelf -= damage;
        if (hardnessSelf <= 0)
            return false;
        if (!hardnessUI) {
            hardnessUI = Instantiate(UIManager.instance.hardnessUIPrefab, transform);
            hardnessIndicator = hardnessUI.transform.GetChild(1);
        }
        float hpWidth = (float) hardnessSelf / hardness;
        float offset = (float) 0.5 * damage / hardness;
        hardnessIndicator.localScale = new Vector3(hpWidth * 7, 0.5f, 1f);
        hardnessIndicator.localPosition -= new Vector3(offset, 0, 0);
        return true;
    }

    public override void Die()
    {
        gameObject.SetActive(false);
        BitMaskTile mask = GetComponent<BitMaskTile>();
        if (mask) {
            mask.UpdateNeighbours();
        }    
        if (softness > 0 && Random.Range(0, 100) < softness) {
            Instantiate(collapse, transform.position, collapse.transform.rotation);
        }
    }
}
