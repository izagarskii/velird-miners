﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public List<Sprite> sprites;
    public SpriteRenderer sRenderer;

    // Start is called before the first frame update
    void Start()
    {
        UpdateSprite();
    }

    public void UpdateSprite()
    {
        sRenderer.sprite = GetSprite();
    }

    protected virtual Sprite GetSprite() {
        return Utils.GetRandom(sprites);
    }
}
