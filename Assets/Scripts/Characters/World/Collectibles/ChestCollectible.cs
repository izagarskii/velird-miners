﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestCollectible : Collectible
{
    public int gold;

    public override void OnCollect(Collider other) {
        TeamManager.instance.ChangeGold(gold);
    }
}
