﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Collectible : MonoBehaviour
{
	public abstract void OnCollect(Collider other);

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Hero")
		{
			OnCollect(other);
			Destroy(gameObject);
		}
	}
}
