﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalCollectible : Collectible
{
    public override void OnCollect(Collider other)
    {
        GameManager.instance.Win();
    }
}
