﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GloryCollectible : Collectible
{
    public int glory;

    public override void OnCollect(Collider other)
    {
        TeamManager.instance.ChangeGlory(glory);
    }
}
