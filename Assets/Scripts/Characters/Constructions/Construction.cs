﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Construction : Actor
{
    public SpriteRenderer sRenderer;
    private int ready = 100;
    private int readiness = 0;

    public abstract void Ready();

    override public bool isPassive()
    {
        return true;
    }

    override protected void ActOn()
    {
        //Task task = new DigTaskFactory().CreateTask(this, actorManager.GetSubject());
        //actorManager.UpdateSubjectTask(task);
        if (readiness < ready)
        {
            Task task = new ConstructTaskFactory().CreateTask(this, ActorManager.instance.GetSubject());
            ActorManager.instance.UpdateSubjectTask(task);
        }
    }

    public bool Construct(int build)
    {
        readiness += build;
        Color color = sRenderer.color;
        color.a = (float) readiness/ready;
        sRenderer.color = color;
        if (readiness >= ready) {
            Ready();
            return false;
        }
        return true;
    }
}
