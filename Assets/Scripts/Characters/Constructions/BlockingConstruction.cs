﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockingConstruction : Construction
{
    public bool isReady = false;
    public GameObject block;

    public override void Ready()
    {
        isReady = true;
        Instantiate(block, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}
