﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombProximityTrap : ProximityTrap {

	public int damage;

	protected override void MakeEffect (Collider subject) {
		subject.GetComponent<Actor>().Suffer(damage);
	}
}
