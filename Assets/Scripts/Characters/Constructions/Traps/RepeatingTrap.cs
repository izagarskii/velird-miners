﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatingTrap : Trap {

	public float fireRate;
	private float nextFire;

	new void Start() {
		base.Start();
		nextFire = GetNextFire();
	}

	// Update is called once per frame
	void Update () {
		if (isReady && Time.time > nextFire)
		{
			nextFire = GetNextFire();
			Vector3 pos = transform.position;
			if (attackPivot) {
				pos = attackPivot.transform.position;
			}
			GameObject obj = Instantiate(attackEffect, pos, attackEffect.transform.rotation);
			obj.transform.Rotate(0, 0, -transform.rotation.eulerAngles.y);
			float radianAngle = transform.rotation.eulerAngles.y * Mathf.PI / 180;
			obj.GetComponent<Effect>().SetDirection(radianAngle);
		}
	}

	private float GetNextFire() {
		return Time.time + fireRate;
	}
}
