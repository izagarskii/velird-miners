﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerTrap : Trap
{
	public float timer;
	public float hotTime;
	public Material hotMaterial;

	private float hotTimeStamp;
	private float produceTimeStamp;

	public new void Start()
	{
		base.Start();
		hotTimeStamp = Time.time + hotTime;
		produceTimeStamp = Time.time + timer;
	}

	public void Update() {
		if (isReady) {
			if (Time.time > hotTimeStamp) {
				sRenderer.material = hotMaterial;
			}
			if (Time.time > produceTimeStamp)
			{
				MakeEffect();
			}
		}
	}

	private void MakeEffect() {
		Instantiate(attackEffect, transform.position, attackEffect.transform.rotation);
		Destroy(gameObject);
	}
}
