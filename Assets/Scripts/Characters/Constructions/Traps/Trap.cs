﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class Trap : Construction {

    public GameObject attackEffect;
    public GameObject attackPivot;
    public bool isReady = false;

    public override void Ready()
    {
        isReady = true;
    }
}
