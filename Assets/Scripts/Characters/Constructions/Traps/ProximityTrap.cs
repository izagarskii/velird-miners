﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ProximityTrap : Trap
{
	public string targetTag;

	protected abstract void MakeEffect(Collider subject);

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == targetTag && isReady)
		{
			MakeEffect(other);
			Instantiate(attackEffect, other.transform.position, Quaternion.Euler(90, 0, 0));
			Destroy(gameObject);
		}
	}
}
