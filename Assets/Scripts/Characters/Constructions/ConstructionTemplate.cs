﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ConstructionTemplate : MonoBehaviour, IPointerClickHandler
{
    public GameObject construction;
    public int cost;
    public bool rotatable;
    private SpriteRenderer sRenderer;
    private BoxCollider bCollider;
    private bool positionIsOk = false;
    private Hero builder;

    private List<Collider> touchers = new List<Collider>();

    void Start() {
        sRenderer = GetComponent<SpriteRenderer>();
        bCollider = GetComponent<BoxCollider>();
    }

	// Update is called once per frame
	void Update () {
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(Snap(pos.x), 0.1f, Snap(pos.z));
        sRenderer.color = positionIsOk ? Color.white : Color.red;
    }

    private bool CheckPosition() {
        return positionIsOk;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject != builder.gameObject) {
            touchers.Add(other);
            positionIsOk = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {    
        touchers.Remove(other);
        if (touchers.Count <= 1) {
            positionIsOk = true;
        }
    }

    public void OnPointerClick(PointerEventData pointer)
    {
        ConstructionManager.instance.CancelConstruction();
        if (pointer.button == PointerEventData.InputButton.Left && positionIsOk && TeamManager.instance.GetGold() >= cost)
        {
            TeamManager.instance.ChangeGold(-cost);
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.Rotate(-90, 0, 0);
            GameObject obj = Instantiate(construction, new Vector3(Snap(pos.x), 0.2f, Snap(pos.z)), transform.rotation);
            Task task = new ConstructTaskFactory()
                .CreateTask(obj.GetComponent<Construction>(), ActorManager.instance.GetSubject());
            ActorManager.instance.UpdateSubjectTask(task);
        }
    }

    public static int Snap(float i) {
        return (int)(i + 0.5);
    }

    public void SetBuilder(Hero builder) {
        this.builder = builder;
    }
}
