﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class Movable : Creature {

    NavMeshAgent agent;

    protected new void Start()
    {
        base.Start();
        agent = GetComponent<NavMeshAgent>();
    }

    public void Move(Vector3 position)
    {
        if (!agent || !agent.isOnNavMesh)
        {
            Debug.Log("AGENT IS CORRUPT");
        }
        else {
            agent.destination = position;
        }
    }

    public bool IsNotMoving() {
        return agent.velocity.Equals(Vector3.zero);
    }
}
