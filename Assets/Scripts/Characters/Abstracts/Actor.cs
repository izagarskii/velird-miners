﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public abstract class Actor : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler {

    private Task task;
    protected ActorManager actorManager;
    protected List<Ability> abilities = new List<Ability>();

    public abstract bool isPassive();

    private void Choose() {
        if (!isPassive()) {
            Debug.Log("Selecting " + name);
            actorManager.SetSubject(this);
			UIManager.instance.SelectActor();
        }
        ConstructionManager.instance.SetUpConstructor(this);
    }

    protected virtual void ActOn() {
        Actor actor = actorManager.GetSubject();
        if (actor) {
            Debug.Log(actor.name + ": I don't know what to do with " + name);
        }
    }

    public virtual void Perform() {
        if (task != null) {
            task.Act();
        }
    }

    public void OnPointerClick(PointerEventData pointer) {
        Debug.Log("clicked " + name);
        switch (pointer.button.ToString()) {
            case "Left":
                Choose();
                break;
            case "Right":
                ActOn();
                break;
        }
    }

    public void GiveOrder(Task t)
    {
        if (task != null) {
            task.EndTask();
        }
        SetTask(t);
        if (task != null) {
            task.StartTask();
        }
    }

    public void AtEase() {
        SetTask(null);
    }

    private void SetTask(Task t) {
        task = t;     
    }

    public void SetNextTask(Task t) {
        if (task != null)
        {
            task.SetNextTask(t);
        }
        else {
            SetTask(t);
        }
    }

    protected void Start()
    {
        actorManager = ActorManager.instance;
    }

    public void OnPointerDown(PointerEventData eventData) { }

    public void OnPointerUp(PointerEventData eventData) { }

	public virtual void ShowStats(ActorStatsWindow statsWindow) {
		statsWindow.actorName.text = name;
		statsWindow.image.sprite = GetComponentInChildren<SpriteRenderer> ().sprite;
		statsWindow.stats.text = "";
	}

    public virtual void Die() {
        actorManager.RemoveActor(this);
        Destroy(gameObject);
    }

    public virtual bool Suffer(int damage) {
        Debug.Log(name + " is being default-suffering");
        return false;
    }

    public virtual int YieldGlory(int killerCharisma) {
        return 0;
    }

    public List<Ability> GetAbilitities()
    {
        return abilities;
    }

    protected bool IsBusy()
    {
        return task.GetsBusy();
    }
}
