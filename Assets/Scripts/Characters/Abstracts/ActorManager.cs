﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorManager : MonoBehaviour {

    public static ActorManager instance;

    private List<Actor> actors;
    private Actor subject;
    private AbstractObjectlessTaskFactory taskFactory;

    void Awake () {
        if (instance == null)
        {
            instance = this;
        }
        SetTaskFactory(new MoveTaskFactory());
        UpdateActorsList();
	}

    public void UpdateActorsList() {
        actors = new List<Actor>(FindObjectsOfType<Actor>());
        actors.RemoveAll((a) => a is Digable);
    }

    public void Act() {
        foreach (Actor a in actors)
        {
            a.Perform();
        }
    }

    public void UpdateSubjectTask(Task task) {
        if (subject)
        {
            subject.GiveOrder(task);
            UIManager.instance.ShowTarget(task.GetObject().transform.position);
        }
    }

    public void UpdateSubjectTask(Vector3 position) {
        if (subject)
        {
			Debug.Log ("Sending " + subject.name + " for a walk");
            Task task = taskFactory.CreateTask(position, subject);
            subject.GiveOrder(task);
            UIManager.instance.ShowTarget(position);
        }
    }

    public void UpdateTaskFactory(string taskFactoryType) { //REFACTORING: string parameter because of UI buttons... maybe transform TaskFactories into MonoBehaviour prefabs?
        switch (taskFactoryType) {
            case "Move":
                SetTaskFactory(new MoveTaskFactory());
                break;
        }
    }

    private void SetTaskFactory(AbstractObjectlessTaskFactory factory) {
        taskFactory = factory;
    }

    public AbstractTaskFactory GetTaskFactory()
    {
        return taskFactory;
    }

    public void SetSubject(Actor s) {
        subject = s;
    }

    public Actor GetSubject()
    {
        return subject;
    }

    public void AddActor(Actor actor) {
        actors.Add(actor);
    }
    public void RemoveActor(Actor actor)
    {
        actors.Remove(actor);
    }

    public List<Actor> GetActors() {
        return actors;
    }
}
