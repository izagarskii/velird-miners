﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActorStatsWindow : MonoBehaviour {

	public Text actorName;
	public Image image;
	public Text stats;
}
