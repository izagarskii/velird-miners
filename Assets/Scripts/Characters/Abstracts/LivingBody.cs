﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivingBody : MonoBehaviour {

    public GameObject healthBar;
    public int maxHealth;

    private Transform hpIndicator;
    private int health;

    public bool Suffer(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            return true;
        }
        float hpWidth = (float) health / maxHealth;
        float offset = (float) 0.5 * damage / maxHealth;
        hpIndicator.localScale = new Vector3(hpWidth * 7, 0.5f, 1f);
        hpIndicator.localPosition -= new Vector3(offset, 0, 0);
        return false;
    }

    void Start()
    {
        hpIndicator = healthBar.transform.GetChild(1);
        health = maxHealth;
    }
}
