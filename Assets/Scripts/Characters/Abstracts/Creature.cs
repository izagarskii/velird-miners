﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Creature : Actor
{
    public Transform abilitiesHolder;
    public List<GameObject> abilitiesToLearn = new List<GameObject>();
    public Transform attackPivot;

    private LivingBody body;

    public abstract void Attack(Actor enemy);

    public abstract float GetAttackRange();

    public bool CheckHit(Actor enemy)
    {
        float distance = Vector3.Distance(transform.position, enemy.transform.position);
        if (distance > GetAttackRange()) {
            return false;
        }

        int wallsLayerMask = 1 << 14;
        RaycastHit hit;
        Vector3 direction = (enemy.transform.position - transform.position) / distance;

        if (!Physics.Raycast(transform.position, direction, out hit, distance, wallsLayerMask))
        {
            return true;
        }
        return false;
    }

    public override bool Suffer(int damage)
    {
        if (body.Suffer(damage))
        {
            Die();
            return true;
        }
        return false;
    }

    protected new void Start()
    {
        base.Start();
        foreach (GameObject a in abilitiesToLearn)
        {
            if (abilitiesHolder)
            {
                Ability ab = Instantiate(a, abilitiesHolder).GetComponent<Ability>();
                ab.SetActor(this);
                abilities.Add(ab);
            }
            else
            {
                Debug.Log("No abilities holder for " + name + " (ability " + a.name + ")");
            }
        }
        body = GetComponent<LivingBody>();
    }

    public int GetMaxHP() {
        body = GetComponent<LivingBody>();
        return body.maxHealth;
    }
}
