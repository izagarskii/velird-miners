﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExposingEnemy : Enemy
{
    public GameObject product;
    public int patience;


    public override void Die()
    {
        base.Die();
        Instantiate(product, transform.position, product.transform.rotation);
    }

    public override void Perform()
    {
        base.Perform();
        if (IsNotMoving()) {
            patience--;
            if (patience < 0) {
                Die();
            }
        }
    }
}
