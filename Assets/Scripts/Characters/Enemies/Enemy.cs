﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Movable {

    public int damage;
    public float attackRange;
    public int spot;
    public int dig;
    public GameObject attackEffect;
    public float glory;
    public GameObject attackVFX;

    public override float GetAttackRange()
    {
        return attackRange;
    }

    public override void Attack(Actor prey)
    {
        Instantiate(attackVFX, attackPivot);
        Instantiate(attackEffect, prey.transform.position, Quaternion.Euler(90, 0, 0));
        prey.Suffer(damage);
    }

    override public bool isPassive()
    {
        return true;
    }

    public override void Die()
    {
        GameManager.instance.enemies.Remove(this);
        base.Die();
    }

    override protected void ActOn()
    {
        Task task = new FightTaskFactory().CreateTask(this, actorManager.GetSubject());
        actorManager.UpdateSubjectTask(task);
    }

    public override int YieldGlory(int killerCharisma)
    {
        return (int) (glory * killerCharisma);
    }

    public override void Perform()
    {
        base.Perform();
        if (Random.Range(0, 1000) < 20) {
            GameObject prefab = GameManager.instance.GetEnemyWailing();
            Instantiate(prefab, transform.position + Vector3.forward, prefab.transform.rotation);
        }
    }
}
