﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour {

    public float spawnRate;
	public float spawnStartDelay;
	public bool mainSpawner;

    public GameObject enemyPrefab;
	public GameObject fastEnemyPrefab;
	public GameObject toughEnemyPrefab;
	public Text startDelayCountdown;
	public Vector3 spawnDirection = Vector3.right;

	private float nextSpawn;
	private bool started;

	private void Start()
	{
		nextSpawn = Time.time + spawnStartDelay;
	}

	// Update is called once per frame
	void Update () {
		if (Time.time > nextSpawn && !GameManager.instance.gameIsOver)
		{
			Spawn();
			UpdateNextSpawn();
			if (!started) {
				started = true;
				startDelayCountdown.gameObject.SetActive(false);
			}
			
		}

		if (mainSpawner && !started)
		{
			startDelayCountdown.text = "" + (int)(nextSpawn - Time.time);
		}
	}

	private void Spawn() {
		if (GameManager.instance.enemies.Count < 20) {
			GameObject prefab = enemyPrefab;
			if (Random.Range(1, 100) > 80)
			{
				prefab = fastEnemyPrefab;
			}
			else if (Random.Range(1, 100) > 90)
			{
				prefab = toughEnemyPrefab;
			}
			int wallsLayerMask = 1 << 14;
			RaycastHit hit;
			if (Physics.Raycast(transform.position, spawnDirection, out hit, 1, wallsLayerMask))
			{
				Digable wall = hit.collider.gameObject.GetComponent<Digable>();
				wall.Die();
			}
			Enemy enemy = Instantiate(prefab, transform.position + spawnDirection, Quaternion.identity).GetComponent<Enemy>();
			enemy.GiveOrder(new HuntTask(enemy));
			ActorManager.instance.AddActor(enemy);
			GameManager.instance.enemies.Add(enemy);
		}
	}
	
	private void UpdateNextSpawn()
	{
		nextSpawn = Time.time + spawnRate;
	}
}
