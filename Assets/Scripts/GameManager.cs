﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public bool forceStart = false;
    public GameObject gameOverScreen;
    public GameObject winScreen;
    public GameObject enemyDialogueBubblePrefab;
    public List<string> enemyWails;

    public float gameSpeed;

    public static GameManager instance;
    public List<Hero> heroes;
    public List<Enemy> enemies;
    public bool gameIsOver = false;
    private bool gameStarted = false;

    private float nextTick;

    // Use this for initialization
    void Awake () {
        if (instance == null) {
            instance = this;
        }
	}

    private void Start()
    {
        if (forceStart) {
            StartGame();
            Debug.Log("Game started in FORCED mode");
        }
    }

    public void StartGame() {
        heroes = new List<Hero>(FindObjectsOfType<Hero>());
        foreach (Hero hero in heroes)
        {
            hero.GiveOrder(new GuardTask(hero));
        }
        enemies = new List<Enemy>(FindObjectsOfType<Enemy>());
        foreach (Enemy enemy in enemies) {
            enemy.GiveOrder(new HuntTask(enemy));
        }
        ActorManager.instance.UpdateActorsList();
        gameStarted = true;
    }

    void Update()
    {
        if (gameStarted && !gameIsOver && Time.time > nextTick)
        {
            ActorManager.instance.Act();
            UpdateNextTick();
            if (heroes.Count <= 0) {
                gameOverScreen.SetActive(true);
                gameIsOver = true;
            }
        }
    }

    public void Win()
    {
        winScreen.SetActive(true);
        gameIsOver = true;
    }

    public bool CheckCurrentActor(Actor actor) {
        return ActorManager.instance.GetSubject() != null && ActorManager.instance.GetSubject().Equals(actor);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Restart()
    {
        SceneManager.LoadScene(1);
    }

    public GameObject GetEnemyWailing() {
        enemyDialogueBubblePrefab.GetComponentInChildren<Text>().text = Utils.GetRandom(enemyWails);
        return enemyDialogueBubblePrefab;
    }

    private void UpdateNextTick()
    {
        nextTick = Time.time + gameSpeed;
    }
}
