﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerProducer : MonoBehaviour
{
	public float timer;
	public float hotTime;
	public Color hotColor;
	public GameObject[] products;
	public SpriteRenderer sRenderer;

	private float hotTimeStamp;
	private float produceTimeStamp;

	public void Start()
	{
		hotTimeStamp = Time.time + hotTime;
		produceTimeStamp = Time.time + timer;
	}

	public void Update()
	{
		if (Time.time > hotTimeStamp)
		{
			sRenderer.color = hotColor;
		}
		if (Time.time > produceTimeStamp)
		{
			Produce();
		}
	}

	private void Produce()
	{
		foreach (GameObject p in products) {
			Instantiate(p, transform.position, p.transform.rotation);
		}
		Destroy(gameObject);
	}
}
