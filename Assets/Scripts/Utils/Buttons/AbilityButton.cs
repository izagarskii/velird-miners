﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityButton : MonoBehaviour
{
    public Image image;
    public Text label;
    public Text cooldownLabel;

    private Button button;
    private Ability ability;

    public void Start()
    {
        button = GetComponent<Button>();
    }

    public void SetAbility(Ability ability) {
        this.ability = ability;
        image.sprite = ability.sprite;
        label.text = ability.abilityName;
    }

    public void Activate() {
        ability.Activate();
    }

    public void Update()
    {
        int tick = ability.Tick();
        cooldownLabel.text = "";
        if (tick > 0)
        {
            cooldownLabel.text += tick;
            button.interactable = false;
        }
        else {
            button.interactable = true;
        }
    }
}
