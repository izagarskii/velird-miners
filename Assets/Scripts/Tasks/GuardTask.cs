﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardTask : Task {

    private Hero guardian;
    private Enemy enemy;
    private float nextFire;

    public GuardTask(Actor subj)
    {
        SetSubject(subj);
        guardian = GetSubject() as Hero;
    }

    override protected void Perform()
    {
        if (!enemy)
        {
            WatchOut();
        }
        else {
            if (Time.time > nextFire)
            {
                guardian.Attack(enemy);
                UpdateNextFire();
            }
        }
    }

    private void WatchOut() {
        float closest = 999;
        foreach (Enemy e in GameManager.instance.enemies)
        {
            if (!guardian.CheckHit(e)) {
                continue;
            }
            float distance = Vector3.Distance(guardian.transform.position, e.transform.position);
            if (distance < closest)
            {
                closest = distance;
                enemy = e;
            }
        }
    }

    private void UpdateNextFire()
    {
        nextFire = Time.time + guardian.GetFireRate();
    }
}
