﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DigTask : Task {

    private Hero digger;

    public DigTask(Actor subj)
    {
        SetSubject(subj);
        digger = GetSubject() as Hero;
    }

    override protected void Perform()
    {
        Digable rock = (Digable) GetObject().GetComponent<Actor>();
        if (!rock.Dig(digger.pick))
        {
            rock.Die();
            GetSubject().GiveOrder(GetNextTask());
            EndTask();
        }
        else {
            Object.Instantiate(digger.attackEffect, rock.transform.position, Quaternion.Euler(90, 0, 0));
        }
    }
}
