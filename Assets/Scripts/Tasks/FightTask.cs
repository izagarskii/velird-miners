﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightTask : Task
{
    private Hero fighter;
    private float nextFire;

    public FightTask(Actor subj)
    {
        SetSubject(subj);
        fighter = GetSubject() as Hero;
    }

    override protected void Perform()
    {
        if (!GetObject())
        {
            GetSubject().GiveOrder(GetNextTask());
            EndTask();
        }
        else
        {
            if (Time.time > nextFire) {
                Enemy enemy = GetObject().GetComponent<Enemy>();
                if (fighter.CheckHit(enemy))
                {
                    fighter.Attack(enemy);
                    UpdateNextFire();
                }
            }
        }
    }

    private void UpdateNextFire() {
        nextFire = Time.time + fighter.GetFireRate();
    }
}
