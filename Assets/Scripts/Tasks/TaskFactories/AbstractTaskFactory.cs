﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractTaskFactory {

    abstract public GameObject GetTargetUIPrefab();
}
