﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DigTaskFactory : AbstractTaskFactory {

    public float pickReach = 1.05f;

    public Task CreateTask(Actor obj, Actor subject)
    {
        if (subject && obj is Digable)
        {
            MoveTask approach = new MoveTask(subject, obj.transform.position);
            approach.SetRange(pickReach);
            approach.SetObject(obj);
            Task dig = new DigTask(subject);
            dig.SetObject(obj);
            Task aware = new GuardTask(subject);
            dig.SetNextTask(aware);
            approach.SetNextTask(dig);
            return approach;
        }
        return null;
    }

    public override GameObject GetTargetUIPrefab()
    {
        return UIManager.instance.digTargetUIPrefab;
    }
}
