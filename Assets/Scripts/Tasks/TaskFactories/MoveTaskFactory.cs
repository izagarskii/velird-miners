﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTaskFactory : AbstractObjectlessTaskFactory{

    override public Task CreateTask(Vector3 position, Actor subject)
    {
        Task t = new MoveTask(subject, position);
        Task aware = new GuardTask(subject);
        t.SetNextTask(aware);
        return t;
    }

    public override GameObject GetTargetUIPrefab()
    {
        return UIManager.instance.moveTargetUIPrefab;
    }
}
