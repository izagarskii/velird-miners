﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstructTaskFactory : AbstractTaskFactory
{
    public float hammerReach = 1f;

    public Task CreateTask(Construction construction, Actor builder)
    {
        if (builder)
        {
            MoveTask approach = new MoveTask(builder, construction.transform.position);
            approach.SetRange(hammerReach);
            approach.SetObject(construction);
            Task build = new ConstructTask(builder);
            build.SetObject(construction);
            Task aware = new GuardTask(builder);
            build.SetNextTask(aware);
            approach.SetNextTask(build);
            return approach;
        }
        return null;
    }

    public override GameObject GetTargetUIPrefab()
    {
        return UIManager.instance.digTargetUIPrefab;
    }
}
