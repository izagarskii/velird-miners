﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardTaskFactory : AbstractObjectlessTaskFactory{

    override public Task CreateTask(Vector3 position, Actor subject) {
        GuardTask t = new GuardTask(subject);
        return t;
    }

    public override GameObject GetTargetUIPrefab()
    {
        return UIManager.instance.guardTargetUIPrefab;
    }
}
