﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightTaskFactory : AbstractTaskFactory
{
    public Task CreateTask(Actor obj, Actor subject)
    {
        if (subject && obj is Enemy && subject is Hero)
        {
            Hero hero = subject as Hero;
            MoveTask approach = new MoveTask(subject, obj.transform.position);
            approach.SetRange(hero.GetAttackRange());
            approach.SetObject(obj);
            Task fight = new FightTask(subject);
            fight.SetObject(obj);
            Task aware = new GuardTask(subject);
            fight.SetNextTask(aware);
            approach.SetNextTask(fight);
            return approach;
        }
        return null;
    }

    public override GameObject GetTargetUIPrefab()
    {
        return UIManager.instance.digTargetUIPrefab;
    }
}
