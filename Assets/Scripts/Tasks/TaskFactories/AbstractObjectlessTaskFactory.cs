﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractObjectlessTaskFactory : AbstractTaskFactory {

    abstract public Task CreateTask(Vector3 position, Actor subject);
}
