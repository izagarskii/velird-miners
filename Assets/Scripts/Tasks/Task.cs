﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Task {

    private Actor subject;
    private Actor obj;
    private Task nextTask;

    public void SetNextTask(Task t) {
        nextTask = t;
    }

    public Task GetNextTask() {
        Debug.Log("GETTING NEXT TASK: " + nextTask);
        return nextTask;
    }

    public void SetSubject(Actor s)
    {
        subject = s;
    }
    public Actor GetSubject() {
        return subject;
    }

    public void SetObject(Actor o)
    {
        obj = o;
    }
    public Actor GetObject()
    {
        return obj;
    }

    public void StartTask()
    {
        
    }

    public virtual void EndTask() {

    }

    public void Act() {
        if (GetSubject()) {
            Perform();
        }
    }

    public virtual bool GetsBusy() {
        return true;
    }

    abstract protected void Perform();
    
}
