﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTask : Task {
    private Vector3 destination;
    private float acceptableRange = 0.15f;

    public MoveTask(Actor subj, Vector3 pos) {
        destination = pos;
        SetSubject(subj);
        Hero hero = GetSubject() as Hero;
        hero.Move(destination);
    }

    public void SetRange(float range) {
        acceptableRange = range;
    }

    override protected void Perform() {
        CheckFinish();
    }

    private void CheckFinish()
    {
        /*if (GameManager.instance.CheckCurrentActor(GetSubject())) {
            Debug.Log("Move Distance: " + Vector3.Distance(GetSubject().transform.position, destination));
            Debug.Log("Moving from " + GetSubject().transform.position + " to " + destination);
        }*/
        if (Vector3.Distance(GetSubject().transform.position, destination) <= acceptableRange)
        {
            GetSubject().GiveOrder(GetNextTask());
            EndTask();
        }
    }

    public override bool GetsBusy()
    {
        return false;
    }
}
