﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HuntTask : Task {

    private Hero prey;
    private Enemy hunter;

    public HuntTask(Actor subj)
    {
        SetSubject(subj);
        hunter = GetSubject() as Enemy;
        ChoosePrey();
    }

    override protected void Perform()
    {
        ChoosePrey();
        if (prey)
        {
            hunter.Move(prey.transform.position);
            if (hunter.CheckHit(prey))
            {
                hunter.Attack(prey);
            }
            if (hunter.dig > 0) {
                if (hunter.IsNotMoving()) {
                    CrushObstacles();
                }
            }
        }
    }

    private void ChoosePrey() {
        if (GameManager.instance.heroes.Count > 0) {
            prey = GameManager.instance.heroes[0];
            if (prey && GameManager.instance.heroes.Count > 1)
            {
                float delta = (GetSubject().transform.position - prey.transform.position).magnitude;
                foreach (Hero person in GameManager.instance.heroes)
                {
                    if ((GetSubject().transform.position - person.transform.position).magnitude < delta)
                    {
                        prey = person;
                        delta = (GetSubject().transform.position - prey.transform.position).magnitude;
                    }
                }
                if (hunter.spot > 0 && delta > hunter.spot) {
                    prey = null;
                }
            }
        }
    }

    private void CrushObstacles() {
        Vector3[] directions = { Vector3.up, Vector3.down, Vector3.forward, Vector3.back };
        RaycastHit hit;
        int layerMask = 1 << 14;

        Digable toCrush = null;
        float curDistance = 99999f;
        foreach (Vector3 dir in directions) {
            if (Physics.Raycast(hunter.transform.position, dir, out hit, 2f, layerMask)) {
                Digable pretendent = hit.collider.gameObject.GetComponent<Digable>();
                if (pretendent) {
                    if (toCrush == null || curDistance > Vector3.Distance(pretendent.transform.position, hunter.transform.position))
                    {
                        toCrush = pretendent;
                        curDistance = Vector3.Distance(toCrush.transform.position, hunter.transform.position);
                    }
                }
            }
        }
        if (toCrush != null) {
            if (!toCrush.Dig(hunter.dig))
            {
                toCrush.Die();
            }
            else
            {
                Object.Instantiate(hunter.attackEffect, toCrush.transform.position, Quaternion.Euler(90, 0, 0));
            }
        }
    }
}
