﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstructTask : Task
{
    private Hero builder;

    public ConstructTask(Actor subj)
    {
        SetSubject(subj);
        builder = GetSubject() as Hero;
    }

    override protected void Perform()
    {
        Construction construction = (Construction)GetObject().GetComponent<Actor>();
        if (!construction.Construct(builder.build))
        {
            GetSubject().GiveOrder(GetNextTask());
            EndTask();
        }
        else
        {
            Object.Instantiate(builder.attackEffect, construction.transform.position, Quaternion.Euler(90, 0, 0));
        }
    }
}
